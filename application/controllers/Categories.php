<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends CI_Controller {
	public function get_cat_sub_cat()
	{
		$this->load->model('model_categories');
		$cat = $this->model_categories->get_cat_sub_cat();
		echo json_encode($cat);
	} 
}
