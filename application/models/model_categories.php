<?php

class model_categories extends CI_Model {

        public $title;
        public $content;
        public $date;

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

        public function get_cat_sub_cat()
        {
                $query = $this->db->query('SELECT categories.id as id_cat,categories.title as title_cat,sub_categories.id as id_sub_cat,sub_categories.title as title_sub_cat,sub_categories.cat_id as cat_id_sub_cat FROM `categories` left join sub_categories on categories.id = sub_categories.cat_id');
                $a = $query->result_array();
                $cat = array();
                for($i=0;$i<count($a);$i++){ 
                        $temp = array();$checkexist = false;
                        for($j=0;$j<count($cat);$j++)
                        {
                                if($a[$i]['id_cat']==$cat[$j]['id'])
                                {
                                        $checkexist = true;             
                                        break;
                                }
                        }
                        if(!$checkexist){
                                $temp['id'] = $a[$i]['id_cat'];
                                $temp['title'] = $a[$i]['title_cat'];
                                $temp_sub_cat = array();
                                for($k=0;$k<count($a);$k++){
                                        if($a[$i]['id_cat']==$a[$k]['cat_id_sub_cat'])
                                                array_push($temp_sub_cat,array('id'=>$a[$k]['id_sub_cat'],'title'=>$a[$k]['title_sub_cat']));
                                }
                                $temp['subcat'] = $temp_sub_cat;
                                array_push($cat,$temp);
                        }
                }
                return $cat;

        }

        public function insert_entry()
        {
                $this->title    = $_POST['title']; // please read the below note
                $this->content  = $_POST['content'];
                $this->date     = time();

                $this->db->insert('entries', $this);
        }

        public function update_entry()
        {
                $this->title    = $_POST['title'];
                $this->content  = $_POST['content'];
                $this->date     = time();

                $this->db->update('entries', $this, array('id' => $_POST['id']));
        }

}


?>